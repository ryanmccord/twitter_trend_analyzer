# README

Rick and the Tunespeak Team,

Thank you for the opportunity to do this small project!  It was a good 
opportunity for myself to learn and experience another programming 
language.

In post mortem, the Search model really has no point.  More could be done 
with it if, in point, the search data was displayed on a 'search history' 
type of page.  I purposefully did not go any further with that piece of 
the project.

The most challenging part for me was getting the routing to make sense.
Ruby makes a lot of assumptions, and the routing was probably the only
part I had a time wrapping my head around.

This is the first time I have used the Twitter API.  My only experience 
with social media APIs is when using Facebook's.  I found Twitter's easy 
to use, but felt there was limited documentation regarding the gem that 
was used.

I do know that in the ral world, it would be best practice to obfuscate
the auth keys for the Twitter client.  I am not a Twitter user nor have 
I ever been.  I will more than likely disable the account following
the appraisal of this project.  

Please see the notes below regarding system dependencies and how to run 
the application.

* System dependencies
	
	- 	Ruby v2.4.4
	- 	Rails > 5.2.0
	- 	Puma > 3.11
	- 	sqlite3 gem
	- 	twitter gem

* Database creation

	- 	on first start up, it may be necessary to run 'rake db:schema:load'
		but should not have to

* How to run

	- 	navigate to the project 'bin' folder
	-	run 'rails server' to start the local server
	- 	navigate to localhost:3000/searches/new
	- 	input a query to search
	- 	two tables are populated showing User Mentions and Hashtags 
		related to the search 

* Other notes

	- 	the search option for the Twitter search is 'popular' as stated
		as a requirement in the project scope
	- 	only 100 tweets are collected
	- 	Twitter Bootstrap is a dependency of proper UI layout
	- 	server-side validation for query input is used
	- 	there is no javascript applied and all back end procedures were
		accomplished using the ruby programming language

Thank you,

J Ryan McCord