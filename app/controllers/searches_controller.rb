class SearchesController < ApplicationController
	def new
		#instantiate new Search instance variable
  		@search = Search.new
  	end

  	def create
  		#instantiate new Search instance variable
  		@search = Search.new(params.require(:search).permit(:query))

  		#check to see if any errors were thrown server-side
  		#if error, set flash[:notice] and redirect_back
  		#else, start the Twitter search
  		if !@search.save
  			flash[:notice] = "ERROR: check your input"
  			redirect_back(fallback_location: root_path)
		else
			#collect the popular tweets, limit count to 100
			begin
			  	tweets = Twitter::Client::Instance.search(@search.query, result_type: "popular").take(100).collect 
			rescue Twitter::Error::Forbidden => e
				flash[:notice] = "ERROR: Twitter search input error"
  			end

			#check to see of there are any tweets collected
			if !tweets.nil? && tweets.count > 0
				#for each tweet, check for any user mentions and hashtags
				#if there are any, place them into their respective hash
				#and keep count, not allowing any duplicates
				#if there are no user mentions or hashtags, empty the variable
				tweets.each do |tweet|
					if !tweet.user_mentions.empty?
						tweet.user_mentions.each do |i| 
							if Twitter::Results.userMentions.key?("@" + i.screen_name)
								Twitter::Results.userMentions["@" + i.screen_name] += 1
							else
								Twitter::Results.userMentions["@" + i.screen_name] = 1
							end #if-else
						end #each
					else
						Twitter::Results.userMentions.clear
					end #if-else
					if !tweet.hashtags.empty?
						tweet.hashtags.each do |i|
							if Twitter::Results.hashTags.key?("#" + i.text)
								Twitter::Results.hashTags["#" + i.text] += 1
							else
								Twitter::Results.hashTags["#" + i.text] = 1
							end #if-else
						end #each
					else
						Twitter::Results.hashTags.clear
					end
				end #if-else
			else
				Twitter::Results.userMentions.clear
				Twitter::Results.hashTags.clear
			end #if

			#Go ahead and sort the hashes in descending order for view display
			Twitter::Results.sort_hash
			
			#hack to pass the search query into the view
			session[:query] = @search.query
			
			#once complete, redirect back to the same page
			redirect_back fallback_location: { action: "new" }
		end	  
	end
end
