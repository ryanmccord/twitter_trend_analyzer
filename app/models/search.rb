class Search < ApplicationRecord
	validates :query, presence: true, 
		length: { minimum: 1 }
end