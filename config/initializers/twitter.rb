module Twitter
	#The purpose of this class is to allow the initialization
	#of the Twitter Rest Client variable, named 'Instance' in
	#this case
	class Client
		require 'twitter'

		Instance = Twitter::REST::Client.new do |config|
		 		config.consumer_key = 'HWzohRHdXlLsmwgR02dsMBU30'
		  		config.consumer_secret = 'ZF94HO9l02gRH2DwgAo2XlAr5zdxWZDWLBkWrkbExGBtcf9zlA'
		  		config.access_token = '993509270363701249-1zkWBL0Z1dlE4vkp2x1o2TgsoG0Hb4C'
		  		config.access_token_secret = '9IKXPoOICnY4xEl8NaQXR7gy10CNQZwMO593IGeCEYKFo'
		end
	end

	#This class will allow the use of two Hash data structures
	#to be used globally in the application.  These Hashes will
	#store the results from the Twitter search queries
	class Results
		@userMentions = Hash.new
		@hashTags = Hash.new

		#Define class accessors
		class << self
		 	attr_accessor :userMentions
		 	attr_accessor :hashTags
		end

		#Class method used to sort the hashes in descending order
		def self.sort_hash
			temp = @userMentions.sort_by {|k,v|v}.reverse
			@userMentions = Hash[temp]
			temp = @hashTags.sort_by {|k,v|v}.reverse
			@hashTags = Hash[temp]
		end
	end
end