Rails.application.routes.draw do
  get 'searches/new'

  resources :searches

  root 'searches#new'
end
